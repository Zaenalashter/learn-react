import React from 'react';

const makanan = [
    {
        nama: 'Soto',
        harga: 12000
    },
    {
        nama: 'Bakso',
        harga: 10000
    },
    {
        nama: 'Mie Ayam',
        harga: 7000
    },
    {
        nama: 'Nasi Goreng',
        harga: 15000
    },
];

//Reduce
const total = makanan.reduce((total_harga, r) => {
    return total_harga+r.harga;
}, 0);

const Map = () => {
  return <div>
            <h2>Map Sederhana</h2>
            <ul>
                {makanan.map((r, index) => (
                    <li>{index+1}.{r.nama} - {r.harga}</li>
                ))}
            </ul>

            <h2>Map Filter Harga yang lebih dari 11.000</h2>
            <ul>
                {makanan
                    .filter((r) => r.harga > 11000) 
                    .map((r, index) => (
                        <li>{index+1}.{r.nama} - {r.harga}</li>
                    ))
                }
            </ul>

            <h2>Total Harga : {total}</h2>
        </div>;
};

export default Map;
